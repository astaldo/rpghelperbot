#!/usr/bin/python2
# -*- coding:utf8 -*-


TOKEN = ''  # Bot token code


def get_bot_token():
    token_file = open('token.txt', 'r')
    token = token_file.read()
    token_file.close()
    return token


def init():
    global TOKEN

    # Gets and sets the token code
    TOKEN = get_bot_token()

if __name__ == "__main__":
    init()
